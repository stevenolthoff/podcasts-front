export namespace Spotify {
  export interface ExternalUrls {
    spotify: string
  }

  export interface Image {
    height: number
    url: string
    width: number
  }

  export interface Item {
    audio_preview_url: string
    description: string
    duration_ms: number
    explicit: boolean
    external_urls: ExternalUrls
    href: string
    id: string
    images: Image[]
    is_externally_hosted: boolean
    is_playable: boolean
    language: string
    languages: string[]
    name: string
    release_date: string
    release_date_precision: string
    type: string
    uri: string
  }

  export interface Episodes {
    href: string
    items: Item[]
    limit: number
    next: string
    offset: number
    previous?: any
    total: number
  }

  export interface Podcast {
    available_markets: string[]
    copyrights: any[]
    description: string
    episodes: Episodes
    explicit: boolean
    external_urls: ExternalUrls
    href: string
    id: string
    images: Image[]
    is_externally_hosted: boolean
    languages: string[]
    media_type: string
    name: string
    publisher: string
    type: string
    uri: string
  }
}
