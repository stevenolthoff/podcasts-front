import Vue from "vue"
import VueRouter from "vue-router"
import Login from "./pages/Login.vue"
import Home from "./pages/Home.vue"
import Search from "./pages/Search.vue"
import Podcast from "./pages/podcast/Podcast.vue"

const routes = [
  {
    path: "/login",
    component: Login,
    name: "Login",
  },
  {
    path: "/",
    component: Home,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/search",
    component: Search,
    name: "Search",
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/podcasts/:id",
    component: Podcast,
    name: "Podcast",
    meta: {
      requiresAuth: true,
    },
  },
]

const router = new VueRouter({
  mode: "history",
  routes,
})

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    const token = Vue.$cookies.get("podcast_api_token")
    if (!token) {
      next({ path: "/login" })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
