import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import Auth from './modules/auth'
import AudioPlayer from './modules/audio_player'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage,
  modules: ['Auth'],
  filter: mutation => ['Auth/setSpotifyAccessToken'].includes(mutation.type)
})

export default new Vuex.Store({
  modules: {
    Auth,
    AudioPlayer
  },
  strict: true,
  plugins: [vuexLocal.plugin]
})
