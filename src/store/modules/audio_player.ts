import { Commit } from 'vuex'

interface State {
  audioUrl: string
  playAudio: boolean
  audioIsLoading: boolean
}

const state: State = {
  audioUrl: '',
  playAudio: false,
  audioIsLoading: false
}

const actions = {
  async startAudio({ commit }: { commit: Commit }, { audioUrl }: { audioUrl: string }) {
    await commit('setPlayAudio', false)
    await commit('setAudioUrl', audioUrl)
    commit('setPlayAudio', true)
  },
  setAudioIsLoading({ commit }: { commit: Commit }, audioIsLoading: boolean) {
    commit('setAudioIsLoading', audioIsLoading)
  }
}

const mutations = {
  setAudioUrl(state: State, audioUrl: string) {
    state.audioUrl = audioUrl
  },
  setPlayAudio(state: State, playAudio: boolean) {
    state.playAudio = playAudio
  },
  setAudioIsLoading(state: State, audioIsLoading: boolean) {
    state.audioIsLoading = audioIsLoading
  }
}

export default {
  namespaced: true,
  state,
  // getters,
  actions,
  mutations
}