import { Commit } from "vuex"
import axios from "axios"
import queryString from "query-string"

interface State {
  spotifyAccessToken: string
}

const state: State = {
  spotifyAccessToken: "",
}

const actions = {
  async setSpotifyAccessToken(
    { commit }: { commit: Commit },
    spotifyAccessToken: string
  ) {
    commit("setSpotifyAccessToken", spotifyAccessToken)
  },
  async refreshSpotifyAccessToken({ commit }: { commit: Commit }) {
    const response = await axios({
      url: `${process.env.VUE_APP_PODCASTS_API}/spotify/access_token`,
      withCredentials: true,
    })
    console.log(response.data)
    commit("setSpotifyAccessToken", response.data.access_token)
  },
}

const mutations = {
  setSpotifyAccessToken(state: State, spotifyAccessToken: string) {
    state.spotifyAccessToken = spotifyAccessToken
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
}
