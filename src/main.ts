import Vue from "vue"
import store from "./store"
import App from "./App.vue"
import vuetify from "./plugins/vuetify"
import VueRouter from "vue-router"
import vueCookies from "vue-cookies"
import router from "./router"

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(vueCookies)

new Vue({
  vuetify,
  router,
  store,
  render: (h) => h(App),
}).$mount("#app")
